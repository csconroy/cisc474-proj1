leftPressed = false;
rightPressed = false;

class CarObject {
	constructor(x, y, width, height, dom) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.dom = dom;
	}

	updateDOM() {
		this.dom.css('left', this.x+'vh');
		this.dom.css('top', this.y+'vh');
	}

	update() {
		if(this.minX !== undefined && this.x < this.minX) {
			this.x = this.minX;
		}
		if(this.maxX !== undefined && this.x > this.maxX) {
			this.x = this.maxX;
		}
		this.updateDOM();
	}
}

class MyCar extends CarObject {
	constructor() {
		super(50, 70, 10, 20, $('#myCar'));
		this.speed = 1;
		this.minX = 0;
		this.maxX = 100 - this.width;
	}

	update() {
		if (leftPressed) {
			this.x -= this.speed;
		}
		if (rightPressed) {
			this.x += this.speed;
		}

		super.update();
	}
}

$(() => {
	const myCar = new MyCar();

	$(document).keydown(e => {
		switch(e.keyCode) {
			case 37:
				leftPressed = true;
				break;
			case 39:
				rightPressed = true;
				break;
		}
	});
	$(document).keyup(e => {
		switch(e.keyCode) {
			case 37:
				leftPressed = false;
				break;
			case 39:
				rightPressed = false;
				break;
		}
	});

	function frame() {
		myCar.update();
		requestAnimationFrame(frame);
	}
	frame();
});
